import numpy as np
import pytest

from narupa.sparrow import SparrowCalculator, EV_PER_HARTREE, ANG_PER_BOHR
from .test_bindings import co_atoms

def test_energy(co_atoms):
    calculator = SparrowCalculator(co_atoms)
    calculator.calculate(properties=['energy'])
    expected_energy = -15.171055082816595 * EV_PER_HARTREE
    assert calculator.results['energy'] == pytest.approx(expected_energy)

def test_forces(co_atoms):
    calculator = SparrowCalculator(co_atoms)
    calculator.calculate(properties=['forces'])
    expected_forces = np.array([[1.1511114191772035e-16, -2.9536552354054594e-17, 0.09145123302131775],
                          [-1.1511114191772035e-16, 2.9536552354054594e-17, -0.09145123302131775]])
    expected_forces *= -1 * EV_PER_HARTREE / ANG_PER_BOHR
    assert np.allclose(calculator.results['forces'], expected_forces)